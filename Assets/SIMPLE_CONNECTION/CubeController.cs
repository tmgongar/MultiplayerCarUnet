﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CubeController : NetworkBehaviour
{
    [SyncVar] public int creatorId;

    public Material orangeMaterial;
    public Material greenMaterial;

    // Start is called before the first frame update
    void Start()
    {
        Material material = creatorId == 1 ? orangeMaterial : greenMaterial;
        GetComponent<MeshRenderer>().material = material;
    }

}
