﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimplePlayerController : NetworkBehaviour
{

    public Material orangeMaterial;
    public Material greenMaterial;

    public float speed = 300f;

    private static int playerIdAvailable;

    [SyncVar]private int playerId;


    public GameObject cubePrefab;


    // Start is called before the first frame update
    void Start()
    {
        Material material = null;

        if(isServer && isLocalPlayer)
        {
            material = orangeMaterial;
        }
        else if (isServer && !isLocalPlayer)
        {
            material = greenMaterial;
        }
        else if (!isServer && isLocalPlayer)
        {
            material = orangeMaterial;
        }
        else if (!isServer && !isLocalPlayer)
        {
            material = greenMaterial;
        }

        GetComponent<MeshRenderer>().material = material;

        if(isServer)
        {
            if(isLocalPlayer)
            {
                playerIdAvailable = 0;
            }
            playerIdAvailable++;

            playerId = playerIdAvailable;
        }

    }

    // Update is called once per frame
    void Update()
    {

        if(isLocalPlayer)
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            transform.Translate(Vector3.forward * (v * Time.deltaTime));
            transform.Rotate(Vector3.up * h * speed * Time.deltaTime);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            CmdSpawnCube();
        }
    }
        

    [Command]
    private void CmdSpawnCube()
    {
        GameObject cube = Instantiate(cubePrefab);
        cube.transform.position = transform.position + (transform.forward * 2);
        cube.GetComponent<CubeController>().creatorId = playerId;
        //cube.GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().material;
        NetworkServer.Spawn(cube);
    }

}
