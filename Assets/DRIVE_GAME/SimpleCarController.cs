﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class WheelInfo
{
    public WheelCollider LeftWheel;
    public WheelCollider RightWheel;
    public bool motor;
    public bool steering;
}




public class SimpleCarController : MonoBehaviour
{

    public List<WheelInfo> wheelInfos;
    public float maxMotorTorque;
    public float maxSteeeringAngle;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeeringAngle * Input.GetAxis("Horizontal");

        bool brake = false;

        if(Input.GetAxis("Vertical") < 0)
        {
            Debug.Log("frenada");
            brake = true;
        }

        foreach (var wheelInfo in wheelInfos)
        {
            if(wheelInfo.steering)
            {
                wheelInfo.LeftWheel.steerAngle = steering;
                wheelInfo.RightWheel.steerAngle = steering;
            }

            if (wheelInfo.motor)
            {
                if (brake == true)
                {
                    wheelInfo.LeftWheel.brakeTorque  = 100000;
                    wheelInfo.RightWheel.brakeTorque = 100000;
                    //wheelInfo.LeftWheel.motorTorque = motor;
                    //wheelInfo.RightWheel.motorTorque = motor;
                }
                else
                {
                    wheelInfo.LeftWheel.brakeTorque = 0;
                    wheelInfo.RightWheel.brakeTorque = 0;

                    wheelInfo.LeftWheel.motorTorque = motor;
                    wheelInfo.RightWheel.motorTorque = motor;
                }


            }

            ApplyLocalPositionToWheels(wheelInfo.LeftWheel);
            ApplyLocalPositionToWheels(wheelInfo.RightWheel);
        }
    }


    private void ApplyLocalPositionToWheels(WheelCollider wheelCollider)
    {
        if(wheelCollider.transform.childCount == 0)
        {
            Debug.LogError("Faltan los modelos de ruedas como hijos directos del wheelCollider...");
            return;
        }

        Transform visualWheel = wheelCollider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;

        wheelCollider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }
}
