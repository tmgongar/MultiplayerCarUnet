﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{

    public float smooth = 0.0f;

    //Ayudante
    public Transform sphereTarget;

    //al que apuntamos
    //public Transform target;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        this.transform.position = Vector3.Lerp(transform.position, sphereTarget.position, Time.deltaTime * smooth);
        this.transform.forward = Vector3.Lerp(transform.forward, sphereTarget.forward, Time.deltaTime * smooth);
    }
}
