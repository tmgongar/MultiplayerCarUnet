﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkSetup : NetworkBehaviour
{
    [SerializeField]
    Behaviour[] componentsToDisable;

    //Camera sceneCamera;

    // Start is called before the first frame update
    void Start()
    {
        if(!isLocalPlayer)
        {
            foreach (var components in componentsToDisable)
            {
                components.enabled = false;
            }

            GetComponentInChildren<Camera>().gameObject.SetActive(false);
        }
        //else
        //{
        //    sceneCamera = Camera.main;

        //}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
